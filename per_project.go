package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/julienschmidt/httprouter"
	"schneider.vip/problem"
)

//PerProjectResponse represents a container for the per-project resultset.
type PerProjectResponse struct {
	Items []PerProject `json:"items"`
}

//PerProject represents one result from the per-project resultset.
type PerProject struct {
	Project     string `json:"project"`
	Access      string `json:"access"`
	Agent       string `json:"agent"`
	Granularity string `json:"granularity"`
	Timestamp   string `json:"timestamp"`
	Views       int    `json:"views"`
}

//PerProjectHandler is the HTTP handler for per-project API requests.
type PerProjectHandler struct {
	logger  *logger.Logger
	session *gocql.Session
}

func (s *PerProjectHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var err error
	var params = httprouter.ParamsFromContext(r.Context())
	var response = PerProjectResponse{Items: make([]PerProject, 0)}

	project := strings.TrimRight(strings.ToLower(params.ByName("project")), ".org")
	access := strings.ToLower(params.ByName("access"))
	agent := strings.ToLower(params.ByName("agent"))
	granularity := strings.ToLower(params.ByName("granularity"))
	var start, end string
	if granularity != "daily" && granularity != "monthly" && granularity != "hourly" {
		problem.New(problem.Status(http.StatusBadRequest), problem.Detail("Invalid granularity")).WriteTo(w)
		return
	}

	if agent != "all-agents" && agent != "automated" && agent != "spider" && agent != "user" {
		problem.New(problem.Status(http.StatusBadRequest), problem.Detail("Invalid agent string")).WriteTo(w)
		return
	}

	if access != "all-access" && access != "desktop" && access != "mobile-app" && access != "mobile-web" {
		problem.New(problem.Status(http.StatusBadRequest), problem.Detail("Invalid access string")).WriteTo(w)
		return
	}

	if start, err = validateTimestamp(params.ByName("start")); err != nil {
		problem.New(problem.Status(http.StatusBadRequest), problem.Detail("Invalid timestamp")).WriteTo(w)
		return
	}
	if end, err = validateTimestamp(params.ByName("end")); err != nil {
		problem.New(problem.Status(http.StatusBadRequest), problem.Detail("Invalid timestamp")).WriteTo(w)
		return
	}

	ctx := context.Background()

	query := fmt.Sprintf(`SELECT v, timestamp FROM "local_group_default_T_pageviews_per_project_v2".data WHERE "_domain" = 'analytics.wikimedia.org' AND project = ? AND access = ? AND agent = ? AND granularity = ? AND timestamp >= ? AND timestamp <= ?`)
	scanner := s.session.Query(query, project, access, agent, granularity, start, end).WithContext(ctx).Iter().Scanner()
	var views int
	var timestamp string

	for scanner.Next() {
		if err = scanner.Scan(&views, &timestamp); err != nil {
			s.logger.Error("Query failed: %s", err)
			problem.New(problem.Status(http.StatusInternalServerError), problem.Detail(err.Error())).WriteTo(w)
		}
		response.Items = append(response.Items, PerProject{
			Project:     project,
			Access:      access,
			Agent:       agent,
			Granularity: granularity,
			Timestamp:   timestamp,
			Views:       views,
		})
	}

	if err := scanner.Err(); err != nil {
		s.logger.Error("Error querying database: %s", err)
		problem.New(problem.Status(http.StatusInternalServerError), problem.Detail(err.Error())).WriteTo(w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	var data []byte
	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		s.logger.Error("Unable to marshal response object: %s", err)
		problem.New(problem.Status(http.StatusInternalServerError), problem.Detail(err.Error())).WriteTo(w)
		return
	}
	w.Write(data)
}
